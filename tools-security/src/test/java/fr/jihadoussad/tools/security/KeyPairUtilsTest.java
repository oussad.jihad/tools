package fr.jihadoussad.tools.security;

import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

class KeyPairUtilsTest {

    @Test
    public void cipherDecipherTest()
            throws NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, InvalidKeySpecException, InvalidKeyException {
        // Generate RSA KeyPair
        final KeyPair keyPair = KeyPairUtils.createRSAKeyPair();

        final String data = "superCipherP@ssw0rd";

        // Cipher data
        final String dataEncoded = KeyPairUtils.cipher(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()), data);

        // Test decipher data
        assertThat(KeyPairUtils.decipher(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()), dataEncoded))
                .isEqualTo(data);
    }
}
