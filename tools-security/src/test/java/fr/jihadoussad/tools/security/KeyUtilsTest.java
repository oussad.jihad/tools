package fr.jihadoussad.tools.security;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

public class KeyUtilsTest {

    @Test
    public void stringSecretToKeyTest() throws NoSuchAlgorithmException {
        // Create HMAC-SHA256 secret key
        final String keyString = KeyUtils.keyToString(KeyUtils.createHmacSHA256Key());

        assertThat(Base64.getEncoder().encodeToString(KeyUtils.stringToKey(keyString).getEncoded()))
                .isEqualTo(keyString);
    }
}
