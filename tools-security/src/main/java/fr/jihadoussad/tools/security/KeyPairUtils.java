package fr.jihadoussad.tools.security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * KeyPairUtils
 */
public class KeyPairUtils {

    private KeyPairUtils() {}

    /**
     * Generate an asymmetric Key pair with RSA 2048 algorithm
     * @return a key pair
     * @throws NoSuchAlgorithmException when not found actual algorithm
     */
    public static KeyPair createRSAKeyPair() throws NoSuchAlgorithmException {
        final java.security.KeyPairGenerator keyPairGenerator = java.security.KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        return keyPairGenerator.generateKeyPair();
    }

    /**
     * Cipher content with valid RSA 2048 public key
     * @param pubKey the public key
     * @param content the content to cipher
     * @return The content encrypted
     */
    public static String cipher(final String pubKey, final String content)
            throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
        final byte[] x509EncodedBytes = Base64.getDecoder().decode(pubKey.getBytes());

        // extract the public key

        final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(x509EncodedBytes);
        final KeyFactory kf = KeyFactory.getInstance("RSA");
        final PublicKey publicKey = kf.generatePublic(keySpec);

        // cipher content
        final Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        return Base64.getEncoder().encodeToString(cipher.doFinal(content.getBytes()));
    }

    /**
     * Decipher content with valid RSA 2048 private key
     * @param privateKey the private key
     * @param content the content to cipher
     * @return The content encrypted
     */
    public static String decipher(final String privateKey, final String content)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        final byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(privateKey.getBytes());

        // extract the private key

        final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        final KeyFactory kf = KeyFactory.getInstance("RSA");
        final PrivateKey privKey = kf.generatePrivate(keySpec);

        // Decipher content
        final Cipher decipher = Cipher.getInstance("RSA");
        decipher.init(Cipher.DECRYPT_MODE, privKey);

        return new String(decipher.doFinal(Base64.getDecoder().decode(content)));
    }
}

