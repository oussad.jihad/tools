package fr.jihadoussad.tools.security;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * KeyUtils
 */
public class KeyUtils {

    private static final String HMAC_SHA_256 = "HmacSHA256";

    private KeyUtils() {}

    /**
     * Generate a Key with HMAC-SHA256 algorithm
     * @return a Key
     * @throws NoSuchAlgorithmException when not found actual algorithm
     */
    public static Key createHmacSHA256Key() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance(HMAC_SHA_256).generateKey();
    }

    /**
     * Convert a Java Key to encoded base 64 String
     * @param secretKey the Java Key
     * @return the key string encoded in base64
     */
    public static String keyToString(final Key secretKey) {
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    /**
     * Convert an encoded base 64 String to Java Key
     * @param secretKey the key string encoded in base64
     * @return the Java Key
     */
    public static Key stringToKey(final String secretKey) {
        final byte[] decodeKey = Base64.getDecoder().decode(secretKey);

        return new SecretKeySpec(decodeKey, 0, decodeKey.length, HMAC_SHA_256);
    }
}
