# tools

Java library tool box utils.

**State build**: [![pipeline status](https://gitlab.com/oussad.jihad/tools/badges/master/pipeline.svg)](https://gitlab.com/oussad.jihad/tools/-/commits/master)

**Jacoco report**: [![jacoco report](https://gitlab.com/oussad.jihad/tools/badges/master/coverage.svg)](https://gitlab.com/oussad.jihad/tools/-/commits/master)
