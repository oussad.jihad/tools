package fr.jihadoussad.tools.validators;

import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

public class InputValidatorTest {

    /*********************************************** LOGIN *********************************************/

    @Test
    public void short_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("log"))
                .isFalse();
    }

    @Test
    public void long_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("logiiiiiiiiiiiiiiiiin"))
                .isFalse();
    }

    @Test
    public void login_should_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("login"))
                .isTrue();
    }

    @Test
    public void login_with_invalid_character() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("<html>blop</html>"))
                .isFalse();
    }

    @Test
    public void login_with_underscore_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("my_login"))
                .isTrue();
    }

    @Test
    public void login_with_underscore_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("_mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_underscore_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("mylogin_"))
                .isFalse();
    }

    @Test
    public void login_with_dot_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("my.login"))
                .isTrue();
    }

    @Test
    public void login_with_dot_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate(".mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_dot_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("mylogin."))
                .isFalse();
    }

    @Test
    public void login_with_score_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("my-login"))
                .isTrue();
    }

    @Test
    public void login_with_score_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("-mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_score_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("mylogin-"))
                .isFalse();
    }

    @Test
    public void owasp_sql_injection_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.LOGIN.pattern).validate("SELECT * FROM ADMIN"))
                .isFalse();
    }

    /*********************************************** MAIL *********************************************/

    @Test
    public void invalidMailTest() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.MAIL.pattern).validate("test.com"))
                .isFalse();
    }

    @Test
    public void validMailTest() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.MAIL.pattern).validate("test@test.com"))
                .isTrue();
    }

    /*********************************************** PASSWORD *********************************************/

    @Test
    public void password_with_more_than_32_characters() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("Abcdefghijklm-nopqrstuvwxyz123456"))
                .isFalse();
    }

    @Test
    public void password_with_less_than_8_characters() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("Abc-def"))
                .isFalse();
    }

    @Test
    public void password_with_no_upper_case_and_no_special_character() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("abcdefghi"))
                .isFalse();
    }

    @Test
    public void password_with_more_than_2_equal_character() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("Abbbe-fghi"))
                .isFalse();
    }

    @Test
    public void password_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("Abcde-fghi"))
                .isTrue();
    }

    @Test
    public void owasp_sql_injection_password_should_not_be_valid(){
        assertThat(InputValidator.getInstance(CommonRegexValidator.PASSWORD.pattern).validate("Delete * from admin"))
                .isFalse();
    }

    /*********************************************** URL *********************************************/

    @Test
    public void url_invalid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.URL.pattern).validate("heimdall.com"))
                .isFalse();
    }

    @Test
    public void url_http_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.URL.pattern).validate("http://heimdall.com"))
                .isTrue();
    }

    @Test
    public void url_https_valid() {
        assertThat(InputValidator.getInstance(CommonRegexValidator.URL.pattern).validate("https://heimdall.com"))
                .isTrue();
    }
}
