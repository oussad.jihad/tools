package fr.jihadoussad.tools.validators;

import java.util.regex.Pattern;

/**
 * Input validator
 */
public class InputValidator {

    private final Pattern pattern;

    private InputValidator(final Pattern pattern) {
        this.pattern = pattern;
    }

    /**
     * Build the input validator with a specify regex
     * @param pattern the regex pattern
     * @return the input validator
     */
    public static InputValidator getInstance(final Pattern pattern) {
        return new InputValidator(pattern);
    }

    /**
     * Validate an input
     * @param input the input string
     * @return true if input match with the regex, false else
     */
    public boolean validate(final String input) {
        return pattern.matcher(input).matches();
    }
}
