package fr.jihadoussad.tools.api.response;

/**
 * ResponseCode
 */
public enum ResponseCode {

    SUCCESS("00", "Process ok !"),
    INVALID_MAIL("01", "Mail invalid !"),
    MISSING_MANDATORY_FIELD("02", "Missing mandatory field: "),
    INVALID_TOKEN("03", "Token invalid !"),
    EXPIRED_TOKEN("04", "Token expired !"),
    INVALID_SECRET_KEY("05", "Secret key invalid ! Please check if it's correctly encoded. Moreover, the key length should be 256 bits at least."),
    TOKEN_ALREADY_INVALID("06", "The token is already invalid !"),
    APP_NOT_FOUND("07", "Application not exist ! Please verify the application name and try again."),
    APP_EXIST("08", "Application name already exist !"),
    PRIV_EXIST_APP("09", "(Some of) Privilege(s) already exist for this application: "),
    PRIV_NOT_FOUND("10", "Privilege(s) name filled not found ! Please verify it and try again."),
    PRIV_EXIST_USER("11", "(Some of) Privilege(s) already exist for this user : "),
    DEF_PRIV_NOT_DELETED("12", "Default privilege not be able to remove !"),
    USER_NOT_FOUND("13", "User not found ! Please check your login and try again."),
    USER_EXIST("14", "Login already exist !"),
    INVALID_LOGIN("15", "Invalid login ! 4 to 20 character login requiring (only alphanumeric and dot, underscore, score authorized)."),
    INVALID_PASSWORD("16", "Invalid password ! 8 to 32 character password requiring at least.\n" +
            "Moreover, your password should contains one uppercase and special character at least.\n" +
            "Finally, it should contains no more than 2 equal characters"),
    PASSWORD_INCORRECT("17", "Password incorrect !"),
    SAME_PASSWORD("18", "Actual password is the same ! Please use another password"),
    INVALID_PASSWORD_CIPHER("19", "Deciphering password failure ! Please check if you have correctly encrypt it and try again."),
    INVALID_URL("20", "URL invalid !"),
    MAIL_ALREADY_EXIST("21", "Mail already exist !"),
    TECHNICAL_ERROR("99", "Technical error was occurred !");

    public final String code;

    public final String message;

    ResponseCode(final String code, final String message) {
        this.code =  code;
        this.message = message;
    }
}
