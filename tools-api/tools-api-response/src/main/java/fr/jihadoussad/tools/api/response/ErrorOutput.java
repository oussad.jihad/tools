package fr.jihadoussad.tools.api.response;

/**
 * Error response api
 */
public class ErrorOutput {

    private String message;
    private String code;

    public ErrorOutput() {}

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
