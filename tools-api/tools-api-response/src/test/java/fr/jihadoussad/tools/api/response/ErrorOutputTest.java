package fr.jihadoussad.tools.api.response;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorOutputTest {

    @Test
    public void getErrorOutput() {
        final ErrorOutput output = new ErrorOutput();
        output.setMessage(ResponseCode.SUCCESS.message);
        output.setCode(ResponseCode.SUCCESS.code);

        assertThat(output.getCode()).isEqualTo(ResponseCode.SUCCESS.code);
        assertThat(output.getMessage()).isEqualTo(ResponseCode.SUCCESS.message);
    }
}
