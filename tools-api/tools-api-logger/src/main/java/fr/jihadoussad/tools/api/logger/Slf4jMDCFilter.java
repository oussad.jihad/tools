package fr.jihadoussad.tools.api.logger;

import org.slf4j.MDC;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

class Slf4jMDCFilter extends OncePerRequestFilter {

    private final String mdcTokenKey;

    private final String tokenHeader;

    public Slf4jMDCFilter(final String mdcTokenKey, final String tokenHeader) {
        this.tokenHeader = tokenHeader;
        this.mdcTokenKey = mdcTokenKey;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        try {
            final String token;
            if (StringUtils.hasLength(request.getHeader(tokenHeader))) {
                token = request.getHeader(tokenHeader);
            } else {
                token = UUID.randomUUID().toString().toUpperCase().replace("-", "");
            }
            MDC.put(mdcTokenKey, token);
            response.addHeader(tokenHeader, token);
            filterChain.doFilter(request, response);
        } finally {
            MDC.remove(mdcTokenKey);
        }
    }
}
