package fr.jihadoussad.tools.api.logger;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Slf4jMDCFilterConfiguration {

    public static final String DEFAULT_MDC_UUID_TOKEN_KEY = "transaction.id";
    public static final String DEFAULT_TOKEN_HEADER = "X-Header-Token";

    @Bean
    public FilterRegistrationBean<Slf4jMDCFilter> servletRegistrationBean() {
        final FilterRegistrationBean<Slf4jMDCFilter> registrationBean = new FilterRegistrationBean<>();
        final Slf4jMDCFilter log4jMDCFilterFilter = new Slf4jMDCFilter(DEFAULT_MDC_UUID_TOKEN_KEY, DEFAULT_TOKEN_HEADER);
        registrationBean.setFilter(log4jMDCFilterFilter);
        registrationBean.setOrder(2);
        return registrationBean;
    }
}
