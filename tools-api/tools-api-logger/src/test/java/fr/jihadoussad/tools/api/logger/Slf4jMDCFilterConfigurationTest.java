package fr.jihadoussad.tools.api.logger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

import static fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration.DEFAULT_TOKEN_HEADER;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Slf4jMDCFilterConfigurationTest {

    private static final String URL_SERVER = "http://localhost:";

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void loggingWithoutTidInHeaderRequestTest() {
        final ResponseEntity<Void> responseEntity = restTemplate.getForEntity(URL_SERVER + port + "/logging", Void.class);
        assertThat(responseEntity.getHeaders())
                .isNotNull()
                .containsKey(DEFAULT_TOKEN_HEADER);
        final String transactionId = Objects.requireNonNull(responseEntity.getHeaders().get(DEFAULT_TOKEN_HEADER)).get(0);
        assertThat(RestApiTest.appender.containsTransactionId(transactionId)).isTrue();
    }

    @Test
    public void loggingWithTidInHeaderRequestTest() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(DEFAULT_TOKEN_HEADER, "transactionId");
        final ResponseEntity<Void> responseEntity =
                restTemplate.exchange(URL_SERVER + port + "/logging", HttpMethod.GET, new HttpEntity<>(httpHeaders), Void.class);
        assertThat(responseEntity.getHeaders())
                .isNotNull()
                .containsKey(DEFAULT_TOKEN_HEADER);
        assertThat(RestApiTest.appender.containsTransactionId("transactionId")).isTrue();
    }
}
