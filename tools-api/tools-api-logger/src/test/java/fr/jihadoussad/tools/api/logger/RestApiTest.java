package fr.jihadoussad.tools.api.logger;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class RestApiTest {

    public static void main(final String[] args) {
        SpringApplication.run(RestApiTest.class);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private final Logger logger;

    public static MemoryAppender appender;

    public RestApiTest() {
        appender = new MemoryAppender();
        appender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        logger = (Logger) LoggerFactory.getLogger(RestApiTest.class);
        logger.addAppender(appender);
        logger.setLevel(Level.INFO);
        appender.start();
    }

    @GetMapping("/logging")
    public ResponseEntity<Void> logging() {
        logger.info("Hello World !");
        return ResponseEntity.ok().build();
    }
}
