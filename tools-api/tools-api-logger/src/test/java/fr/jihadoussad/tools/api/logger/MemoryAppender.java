package fr.jihadoussad.tools.api.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

import static fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration.DEFAULT_MDC_UUID_TOKEN_KEY;

public class MemoryAppender extends ListAppender<ILoggingEvent> {

    public boolean containsTransactionId(final String transactionId) {
        return this.list.stream()
                .map(ILoggingEvent::getMDCPropertyMap)
                .anyMatch(mdcMap -> transactionId.equals(mdcMap.get(DEFAULT_MDC_UUID_TOKEN_KEY)));
    }
}
