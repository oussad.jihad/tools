package fr.jihadoussad.tools.api.rest;

import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Rest client builder
 */
public class RestClientBuilder {

    private static RestClientBuilder INSTANCE;

    private final RestClient restClient;

    private RestClientBuilder() {
        this.restClient = new RestClient();
    }

    public static RestClientBuilder getInstance() {
        INSTANCE = new RestClientBuilder();
        return INSTANCE;
    }

    /**
     * Set with specific rest template
     * @param restTemplate the specific rest template
     * @return the rest client builder
     */
    public RestClientBuilder withRestTemplate(final RestTemplate restTemplate) {
        restClient.setRestTemplate(restTemplate);
        return INSTANCE;
    }

    /**
     * Build rest client with specific content type http headers
     * Default value application/json
     * @param mediaType the specific content type
     * @return the rest client builder
     */
    public RestClientBuilder contentType(final MediaType mediaType) {
        restClient.getHttpHeaders().setContentType(mediaType);
        return INSTANCE;
    }

    /**
     * Set with specific accepted types http headers
     * Default value application/json only
     * @param mediaTypes the accepted types
     * @return the rest client builder
     */
    public RestClientBuilder acceptTypes(final List<MediaType> mediaTypes) {
        restClient.getHttpHeaders().setAccept(mediaTypes);
        return INSTANCE;
    }

    /**
     * Get the rest client built
     * @return the rest client
     */
    public RestClient build() {
        return restClient;
    }
}
