package fr.jihadoussad.tools.api.rest;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration.DEFAULT_TOKEN_HEADER;

/**
 * Generic Spring rest client
 */
public class RestClient {

    private final HttpHeaders httpHeaders;

    private RestTemplate restTemplate;

    RestClient() {
        this.restTemplate = new RestTemplate();
        // Set headers
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    }

    HttpHeaders getHttpHeaders() {
        return httpHeaders;
    }

    void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Send a restful http request (GET, POST, PUT, DELETE) and get the response
     * @param uri the restful uri api
     * @param httpMethod the http verb (GET, POST, PUT, DELETE)
     * @param type the parametrized output type reference
     * @param <T> the output
     * @return the response entity
     */
    public <T> ResponseEntity<T> getResponseEntity(final String uri, final HttpMethod httpMethod,
                                                   final ParameterizedTypeReference<T> type) {
        return getResponseEntity(uri, httpMethod, Map.of() , type);
    }

    /**
     * Send a restful http request (GET, POST, PUT, DELETE) and get the response
     * @param uri the restful uri api
     * @param httpMethod the http verb (GET, POST, PUT, DELETE)
     * @param requestParams the request parameter
     * @param type the parametrized output type reference
     * @param <T> the output
     * @return the response entity
     */
    public <T> ResponseEntity<T> getResponseEntity(final String uri, final HttpMethod httpMethod,
                                                   final Map<String, String> requestParams,
                                                   final ParameterizedTypeReference<T> type) {
        return getResponseEntity(uri, httpMethod, requestParams, null, type);
    }

    /**
     * Send a restful http request (GET, POST, PUT, DELETE) and get the response
     * @param uri the restful uri api
     * @param httpMethod the http verb (GET, POST, PUT, DELETE)
     * @param <I> the request body
     * @param type the parametrized output type reference
     * @param <O> the output
     * @return the response entity
     */
    public <I, O> ResponseEntity<O> getResponseEntity(final String uri, final HttpMethod httpMethod,
                                                      final I requestBody,
                                                      final ParameterizedTypeReference<O> type) {
        return getResponseEntity(uri, httpMethod, Map.of(), requestBody, type);
    }

    /**
     * Send a restful http request (GET, POST, PUT, DELETE) and get the response
     * @param uri the restful uri api
     * @param httpMethod the http verb (GET, POST, PUT, DELETE)
     * @param requestParams the request parameter
     * @param <I> the request body
     * @param type the parametrized output type reference
     * @param <O> the output
     * @return the response entity
     */
    public <I, O> ResponseEntity<O> getResponseEntity(final String uri, final HttpMethod httpMethod,
                                            final Map<String, String> requestParams, final I requestBody,
                                            final ParameterizedTypeReference<O> type) {
        // Query parameters
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        requestParams.forEach(builder::queryParam);

        // Fill transaction id in httpHeaders
        fillTransactionId();

        HttpEntity<?> entity = new HttpEntity<>(httpHeaders);
        if (requestBody != null) {
            entity = new HttpEntity<>(requestBody, httpHeaders);
        }

        return restTemplate.exchange(builder.build().encode().toUri(), httpMethod,
                entity, type);
    }

    private void fillTransactionId() {
        Optional.ofNullable((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .map(ServletRequestAttributes::getResponse)
                .map(request -> request.getHeader(DEFAULT_TOKEN_HEADER))
                .ifPresent(transactionId -> httpHeaders.add(DEFAULT_TOKEN_HEADER, transactionId));
    }
}
