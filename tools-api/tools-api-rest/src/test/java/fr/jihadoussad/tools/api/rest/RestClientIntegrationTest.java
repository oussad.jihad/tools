package fr.jihadoussad.tools.api.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Collections;
import java.util.Map;

import static fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration.DEFAULT_TOKEN_HEADER;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestClientIntegrationTest {

    private static final String URL_SERVER = "http://localhost:";

    @LocalServerPort
    private int port;

    @Test
    public void helloWorldTest() {
        final String urlRequest = URL_SERVER + port + "/";

        final ResponseEntity<String> response =
                RestClientBuilder.getInstance()
                        .withRestTemplate(new RestTemplate())
                        .contentType(MediaType.APPLICATION_JSON)
                        .acceptTypes(Collections.singletonList(MediaType.APPLICATION_JSON))
                        .build()
                        .getResponseEntity(urlRequest , HttpMethod.GET, new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("hello world");
    }

    @Test
    public void helloWorldWithRequestContext() {
        final String urlRequest = URL_SERVER + port + "/";

        // Mock request holder
        final MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        final MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        mockResponse.addHeader(DEFAULT_TOKEN_HEADER, "transactionId");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockRequest, mockResponse));

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                        .getResponseEntity(urlRequest , HttpMethod.GET, new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
    }

    @Test
    public void helloTest() {
        final String urlRequest = URL_SERVER + port + "/hello";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.GET, Map.of("name", "toto"), new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("hello toto");
    }

    @Test
    public void addParamsTest() {
        final String urlRequest = URL_SERVER + port + "/hello/add-params";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.POST, Map.of("name", "toto"), new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("welcome toto and hello");
    }

    @Test
    public void addBodyTest() {
        final String urlRequest = URL_SERVER + port + "/hello/add-body";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.POST, "toto", new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("welcome toto and hello");
    }

    @Test
    public void addBodyParamsTest() {
        final String urlRequest = URL_SERVER + port + "/hello/add-body-params";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.POST, Map.of("country", "java"), "toto", new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("welcome from java toto and hello");
    }

    @Test
    public void setParamsTest() {
        final String urlRequest = URL_SERVER + port + "/hello/set-params";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.PUT, Map.of("name", "toto"), new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("beautiful like this toto and hello");
    }

    @Test
    public void setBodyTest() {
        final String urlRequest = URL_SERVER + port + "/hello/set-body";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.PUT, "toto", new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("beautiful like this toto and hello");
    }

    @Test
    public void setWithBodyVoidRequestTest() {
        final String urlRequest = URL_SERVER + port + "/hello/set-body-void-request";

        final ResponseEntity<Void> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.PUT, "toto", new ParameterizedTypeReference<Void>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void deleteParamsTest() {
        final String urlRequest = URL_SERVER + port + "/goodbye-params";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.DELETE, Map.of("name", "toto"), new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("goodbye toto");
    }

    @Test
    public void deleteBodyTest() {
        final String urlRequest = URL_SERVER + port + "/goodbye-body";

        final ResponseEntity<String> response = RestClientBuilder.getInstance().build()
                .getResponseEntity(urlRequest, HttpMethod.DELETE, "toto", new ParameterizedTypeReference<String>() {});

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("goodbye toto");
    }
}
