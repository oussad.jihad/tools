package fr.jihadoussad.tools.api.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.tools.api.*")
@RestController
public class RestApiTest {

    public static void main(final String[] args) {
        SpringApplication.run(RestApiTest.class);
    }

    @GetMapping("/")
    public ResponseEntity<String> helloWorld() {
        return ResponseEntity.ok("hello world");
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello(@RequestParam final String name) {
        return ResponseEntity.ok("hello " + name);
    }

    @PostMapping("/hello/add-params")
    public ResponseEntity<String> addWithParams(@RequestParam final String name) {
        return ResponseEntity.ok("welcome " + name + " and hello");
    }

    @PostMapping("/hello/add-body")
    public ResponseEntity<String> addWithBody(@RequestBody final String name) {
        return ResponseEntity.ok("welcome " + name + " and hello");
    }

    @PostMapping("/hello/add-body-params")
    public ResponseEntity<String> addWithBody(@RequestBody final String name, @RequestParam final String country) {
        return ResponseEntity.ok("welcome from " + country + " " + name + " and hello");
    }

    @PutMapping("/hello/set-params")
    public ResponseEntity<String> setWithParams(@RequestParam final String name) {
        return ResponseEntity.ok("beautiful like this " + name + " and hello");
    }

    @PutMapping("/hello/set-body")
    public ResponseEntity<String> setWithBody(@RequestBody final String name) {
        return ResponseEntity.ok("beautiful like this " + name + " and hello");
    }

    @PutMapping("/hello/set-body-void-request")
    public ResponseEntity<Void> setWithBodyVoidRequest(@RequestBody final String name) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/goodbye-params")
    public ResponseEntity<String> deleteWithParams(@RequestParam final String name) {
        return ResponseEntity.ok("goodbye " + name);
    }

    @DeleteMapping("/goodbye-body")
    public ResponseEntity<String> deleteWithBody(@RequestBody final String name) {
        return ResponseEntity.ok("goodbye " + name);
    }
}
